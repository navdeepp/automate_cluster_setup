#this script needs to run on namenode after CMI is created
import os

namenode_ip = raw_input("Enter IP address of namenode: ")
n = raw_input("Enter number of datanodes required for the cluster: ")
datanodes = []
for i in range(int(n)):
    datanodes.append(raw_input("IP address of datanode "+str(i+1)+": "))

#change hostname of namenode
os.system("hostnamectl set-hostname namenode.localdomain")

#edit /etc/hosts file with datanodes and namenodes IP
os.system("echo "+namenode_ip+" namenode.localdomain >> /etc/hosts")
for i in range(len(datanodes)):
    os.system("echo "+datanodes[i]+" datanode"+str(i+1)+".localdomain >> /etc/hosts")

#namenode hadoop conf
    #config files edit

#datanode hadoop conf
    #edit config files
while True:
    y = raw_input("Please press 'y' after manual hadoop config to proceed to install hive")
    if y == 'y' or y == 'Y':
        break
    else:
        print("Please complete hadoop config first")


####### hive installation
'''
install hive
'''
hive_source_url = "http://archive.apache.org/dist/hive/hive-0.10.0/hive-0.10.0-bin.tar.gz"
os.system("cd /root ; curl -O "+hive_source_url)

#extract hive to /usr/local
os.system("tar zxvf /root/hive-* -C /usr/local")
os.system("mv /usr/local/hive-* /usr/local/hive")

#set environment variables for hive in bashrc
os.system("echo '#env var for hive' >> /root/.bashrc")
os.system("echo 'export HIVE_HOME=/usr/local/hive' >> /root/.bashrc")
os.system("echo 'export PATH=$PATH:$HIVE_HOME/bin' >> /root/.bashrc")

os.system("source /root/.bashrc")
#create hive warehouse directory
os.system("mkdir /usr/local/hive/warehouse")
#need to configure and install mysql
#configure hive to use mysql endpoints
while True:
    y = raw_input("Please press 'y' after testting hadoop and hive install to install mysql")
    if y == 'y' or y == 'Y':
        break
    else:
        print("Please test HADOOP and HIVE before installing mysql")
#grab mysql
os.system("yum localinstall -y https://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm")
os.system("yum install -y mysql-server")
#start the mysql service
os.system("systemctl start mysqld.service")
#enable to start at boot time
os.system("systemctl enable mysqld.service")

#print proceed with configuring hive and mysql