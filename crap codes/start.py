import os
import subprocess
from threading import Thread

from utils.utils import pinger

#locate current diretory
print("current directory is :",os.getcwd())

#change current directory to /Applications/VirtualBox.app/Contents/MacOS where VirtualBox executabls are located
os.chdir('/Applications/VirtualBox.app/Contents/MacOS')

print("directory changed to :",os.getcwd())

#get target machine name to connect and convert to common machine image
fresh_machine_name = input("Enter fresh Cent OS vm name: ")
#change this later
fresh_machine_name = 'Cent os'
#find mac address of the machine
mactext = subprocess.check_output("VBoxManage showvminfo '"+fresh_machine_name+"' |grep MAC", shell=True)
mactext = str(mactext, 'utf-8')
macindex = mactext.find('MAC')+5
mac = mactext[macindex:macindex+12]
#mac will be in format 'aabbccddeeff'
#to use this in arp string match we need to change formating
#remove first occourring '0' for pairs eg. 000833340420 to 0:8:33:34:4:20
macf = ''
for i in range(0,12,2):
    if mac[i] == '0':
        macf += mac[i+1]+':'
    else:
        macf += mac[i]+mac[i+1]+':'
macf = macf[0:-1]

#now ping the IP range for every host in network and match ARP tables with MAC
for i in range(256):
    t = Thread(target=pinger, args=(i,))
    t.start()
#match MAC
arp_text = subprocess.check_output("arp -a | grep " + macf,shell=True)
fresh_machine_ip = '?'