# Automate Cluster Setup #

The script in this project will help automating multinode hadoop, hive cluster setup on Virtual Box VM

### What is this repository for? ###

* orchestrating hadoop/hive installation for a local setup/learning for hadoop beginner
### How do I get set up? ###

* Install CentOs on VirtualBox
Run cmi_conf_script.py on the guest os
By default Hadoop source url is configured for hadoop2.7
For fast orchestration use a local image of Hadoop in the folder: /utils/distibution/hadoop/; uncommenting the online install lines
Default verison of hive is 0.10> change the source url for latest versions
For a local image use the path /utils/distibution/hive/
* Configuration
Configurations: The script uses pre-generated conf files at /utils/conf/hadoop or hive> change these if upgrading/changing the versions
* Database configuration
Default password for mysql setup in the script is: "Root@123"
* Deployment instructions
Take machine snapshot
In virtual box save the machine image after the
changes.
From the newly created snapshot create as many
clones needed for datanodes + namenode
Power on the cloned machines Note IP address of the machines
* How to run tests
hive>show databases;
Run a mapreduce program from mapper/reducer at: /utils/lib/mapper.py

### Contribution guidelines ###

Extend to Spark on Yarn
### Who do I talk to? ###

* navdeepp