#configuring cent os for hadoop installation

import os
import subprocess

def cmi_script():
    #remove IPV6 configs from ifcfg network files
    os.system("sed '/IPV6/ d' /etc/sysconfig/network-scripts/ifcfg-enp0s3 | cat > /etc/sysconfig/network-scripts/testfile")
    os.system("mv -f /etc/sysconfig/network-scripts/testfile /etc/sysconfig/network-scripts/ifcfg-enp0s3")
    
    '''
    ssh keygen
    '''
    os.system("ssh-keygen -t rsa")
    os.system("cp /root/.ssh/id_rsa.pub /root/.ssh/authorized_keys")

    '''
    install hadoop and java
    '''
    hadoop_source_url = "http://apache.mirrors.tds.net/hadoop/common/hadoop-2.7.2/hadoop-2.7.2.tar.gz"
    os.system("cd /root ; curl -O "+hadoop_source_url)

    os.system("yum -y install java-1.8.0-openjdk-devel")

    '''
    configure hadoop for common config files for both data node and name node
    '''
    #extract hadoop to /usr/local
    os.system("tar zxvf /root/hadoop-* -C /usr/local")
    os.system("mv /usr/local/hadoop-* /usr/local/hadoop")
   
    #extract java path
    java_path = subprocess.check_output("readlink -f $(which java)", shell=True)
    java_path = java_path[:java_path.find('/jre/bin/java')]

    #set environment variables for java and hadoop in bashrc
    os.system("echo '#env var for java and hadoop' >> /root/.bashrc")
    os.system("echo 'export JAVA_HOME="+java_path+"' >> /root/.bashrc")
    os.system("echo 'export PATH=$PATH:$JAVA_HOME/bin' >> /root/.bashrc")
    os.system("echo 'export HADOOP_HOME=/usr/local/hadoop' >> /root/.bashrc")
    os.system("echo 'export PATH=$PATH:$HADOOP_HOME/bin' >> /root/.bashrc")
    os.system("echo 'export HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop' >> /root/.bashrc")

    os.system("source /root/.bashrc")

    #security alert turning off firewall completely to allow haddop to communicate to namedes and datanodes
    #for hardning purposes configure firewall to only allow certain ports
    os.system("systemctl stop firewalld")
    os.system("systemctl disable firewalld")
    #sync ntp on all machines
    os.system("yum -y install ntp")
    os.system("ntpdate 0.rhel.pool.ntp.org")
cmi_script()